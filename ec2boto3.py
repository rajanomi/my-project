import boto3

# http://boto3.readthedocs.io/en/latest/reference/services/ec2.html#service-resource

ec2 = boto3.resource('ec2', aws_access_key_id='AKIATF264IOQ7PD7HX75',
                     aws_secret_access_key='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',
                     region_name='us-west-2')

# create VPC
vpc = ec2.create_vpc(CidrBlock='10.10.0.0/16')
# we can assign a name to vpc, or any resource, by using tag
vpc.create_tags(Tags=[{"Key": "Name", "Value": "default_vpc"}])
vpc.wait_until_available()
print(vpc.id)

# create then attach internet gateway
ig = ec2.create_internet_gateway()
vpc.attach_internet_gateway(InternetGatewayId=ig.id)
print(ig.id)

# create a route table and a public route
route_table = vpc.create_route_table()
route = route_table.create_route(
    DestinationCidrBlock='0.0.0.0/0',
    GatewayId=ig.id
)
print(route_table.id)

# create subnet
subnet = ec2.create_subnet(CidrBlock='10.10.0.0/24', VpcId=vpc.id)
print(subnet.id)

# associate the route table with the subnet
route_table.associate_with_subnet(SubnetId=subnet.id)

# Create sec group
sec_group = ec2.create_security_group(
    GroupName='shampy_0', Description='shampy_0 sec group', VpcId=vpc.id)
sec_group.authorize_ingress(
    CidrIp='0.0.0.0/0',
    IpProtocol='icmp',
    FromPort=-1,
    ToPort=-1
)
print(sec_group.id)

# find image id  ami-0c2d06d50ce30b442 / us-west-2
# Create instance
instances = ec2.create_instances(
    ImageId= "ami-0c2d06d50ce30b442" , InstanceType='t2.micro', MaxCount=1, MinCount=1,
    NetworkInterfaces=[{'SubnetId': subnet.id, 'DeviceIndex': 0, 'AssociatePublicIpAddress': True, 'Groups': [sec_group.group_id]}])
instances[0].wait_until_running()
print(instances[0].id)